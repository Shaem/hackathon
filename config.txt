Problem 1 (Linux Mint 18 Os):

If there is an error in writing a comand like this
sudo apt-get install arduino

Output:
E: Could not get lock /var/lib/dpkg/lock - open (11: Resource temporarily unavailable)
E: Unable to lock the administration directory (/var/lib/dpkg/), is another process using it?

Solve:
You can delete the lock file with the following command:
sudo rm /var/lib/apt/lists/lock
You may also need to delete the lock file in the cache directory
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock

Problem 2 (Ubuntu 17 Os):

If there is an error when writing a command like this 
sudo apt-get install google-chrome-stable

Output:
Reading package lists... Done
Building dependency tree       
Reading state information... Done
E: Unable to locate package google-chrome-stable

Solve:
There might be some dependency problem. So, what you could do is:
    Download chrome .deb file from official chrome site
    Open terminal in that download folder
    in terminal run these commands:
sudo dpkg -i google-chrome-stable_current_amd64.deb
Here, google-chrome-stable_current_amd64.deb is file name.
sudo apt install -f
sudo dpkg -i google-chrome-stable_current_amd64.deb 
So, the first command will try to install the package. And if there is any dependency problem, then it will fail.
Second command will force apt to automatically install all missing/required dependencies, and the bet part is you don't even need to specify anything, apt will automatically get those.
In 3rd step when you try to install chrome again, now it will be installed successfully.

